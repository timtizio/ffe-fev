<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nodes', function (Blueprint $table) {
            $table->increments('id');
            $table->char('name', 255);
            $table->enum('type', ['D', 'F', 'LD', 'LF']);
            $table->integer('parent_id')->unsigned()->nullable();
            $table->char('path', 255)->nullable();
            $table->integer('user_owner_id')->unsigned();
            $table->integer('group_owner_id')->unsigned();
            $table->char('permissions', 4);
            $table->timestamps();

            $table->foreign('parent_id')->references('id')->on('nodes');
            $table->foreign('user_owner_id')->references('id')->on('users');
            $table->foreign('group_owner_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nodes');
    }
}
