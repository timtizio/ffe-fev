<?php

use App\User;
use App\MetaUser;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Lambda user
        $user = User::create([
            'name' => 'lambda',
            'email' => 'lambda@ffe.fr',
            'password' => '$2y$10$ybSI3naPPTFF1ORPASO.SuwkMLWFfRIDvNFa7/s/rBRBUvoQftPSO', // lambda
        ]);

        // Admin
        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@ffe.fr',
            'password' => '$2y$10$mJ.5xqsuFRkLNG87kIFEAOrJgrrP/Ab5qaJoGZRdSJk9TEcXJvFqe', // admin
        ]);
        MetaUser::create([
            'user_id' => $user->id,
            'key' => 'isAdmin',
            'value' => 'true',
        ]);


    }
}
