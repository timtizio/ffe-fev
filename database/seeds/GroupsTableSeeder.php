<?php

use App\Group;
use App\User;
use Illuminate\Database\Seeder;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lambdaUser = User::where('name', 'lambda')->first();
        $adminUser = User::where('name', 'admin')->first();

        $group1 = Group::create([
            'name' => 'Group 1',
        ]);
        $group2 = Group::create([
            'name' => 'Group 2',
        ]);
        $group3 = Group::create([
            'name' => 'Group 3',
        ]);

        $lambdaUser->groups()->attach($group1->id);
        $adminUser->groups()->attach($group1->id);

        $lambdaUser->groups()->attach($group2->id);

        $adminUser->groups()->attach($group3->id);
    }
}
