<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Node;

class NodesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lambdaUser = User::where('name', 'lambda')->first();
        $adminUser = User::where('name', 'admin')->first();

        $nodes1 = Node::create([
            'name' => 'file1',
            'type' => 'F',
            'path' => 'real/path',
            'user_owner_id' => $lambdaUser->id,
            'group_owner_id' => $lambdaUser->groups()->where('name', 'Group 1')->first()->id,
            'permissions' => '664',
        ]);

        $nodes2 = Node::create([
            'name' => 'directory1',
            'type' => 'D',
            'user_owner_id' => $lambdaUser->id,
            'group_owner_id' => $lambdaUser->groups()->where('name', 'Group 1')->first()->id,
            'permissions' => '775',
        ]);

        $nodes3 = Node::create([
            'name' => 'file2',
            'type' => 'F',
            'parent_id' => $nodes2->id,
            'path' => 'real/path',
            'user_owner_id' => $lambdaUser->id,
            'group_owner_id' => $lambdaUser->groups()->where('name', 'Group 1')->first()->id,
            'permissions' => '664',
        ]);

        $nodes4 = Node::create([
            'name' => 'admin-dir1',
            'type' => 'D',
            'user_owner_id' => $adminUser->id,
            'group_owner_id' => $adminUser->groups()->where('name', 'Group 3')->first()->id,
            'permissions' => '770',
        ]);

        $nodes5 = Node::create([
            'name' => 'admin-file1',
            'type' => 'F',
            'parent_id' => $nodes4->id,
            'path' => 'real/path',
            'user_owner_id' => $adminUser->id,
            'group_owner_id' => $adminUser->groups()->where('name', 'Group 3')->first()->id,
            'permissions' => '660',
        ]);

        $nodes6 = Node::create([
            'name' => 'link1',
            'type' => 'LF',
            'parent_id' => $nodes2->id,
            'path' => '/real/path/of/file1',
            'user_owner_id' => $lambdaUser->id,
            'group_owner_id' => $lambdaUser->groups()->where('name', 'Group 1')->first()->id,
            'permissions' => '664',
        ]);

    }
}
