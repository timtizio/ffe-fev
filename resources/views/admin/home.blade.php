@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <ul class="list-group">
                        <li class="list-group-item admin-panel">
                            Create new <br />
                            <a class="btn btn-primary" href="#" role="button">-> Group</a>
                            <a class="btn btn-primary" href="#" role="button">-> User</a>
                        </li>

                        <li class="list-group-item admin-panel">
                            Settings <br />
                            <a class="btn btn-primary" href="#" role="button">-> Group</a>
                            <a class="btn btn-primary" href="#" role="button">-> User</a>
                        </li>

                        <li class="list-group-item admin-panel">
                            Manage <br />
                            <a class="btn btn-primary" href="#" role="button">-> Add-ons</a>
                            <a class="btn btn-primary" href="#" role="button">-> Plugins</a>
                        </li>

                        <li class="list-group-item admin-panel">
                            Shortcut about last change <br />
                            <a class="btn btn-primary" href="#" role="button">-> Group</a>
                            <a class="btn btn-primary" href="#" role="button">-> User</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
