<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the metadata for the user
     */
    public function metaData() {
        return $this->hasMany('App\MetaUser');
    }

    /**
     * Return if the user is admin or not
     */
    public function isAdmin() {
        $isAdmin = $this->metaData()->where('key', 'isAdmin')->first();

        if( $isAdmin == null )
            return false;

        return $isAdmin->value == 'true' ? true : false;
    }

    /**
     * Return the groups where the user is member
     */
    public function groups() {
        return $this->belongsToMany('App\Group');
    }

    /**
     * Return the user own nodes
     */
    public function getOwnNodes() {
        return $this->hasMany('App\Node', 'user_owner_id');
    }
}
