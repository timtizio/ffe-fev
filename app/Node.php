<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Node extends Model
{
    protected $fillable = [
        'name', 'type', 'parent_id', 'path', 'user_owner_id', 'group_owner_id', 'permissions',
    ];

    public function getParent() {
        return $this->belongsTo('App\Node', 'parent_id')->first();
    }

    public function getChildren() {
        return $this->hasMany('App\Node', 'parent_id');
    }

    public function getUserOwner() {
        return $this->belongsTo('App\User', 'user_owner_id')->firstOrFail();
    }

    public function getGroupOwner() {
        return $this->belongsTo('App\Group', 'group_owner_id')->firstOrFail();
    }
}

