<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AdminController extends Controller
{
    /**
     * Show the application admin home.
     *
     * @return \Illuminate\Http\Response
     */
    public function home() {
        return view('admin/home');
    }
}
