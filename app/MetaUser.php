<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetaUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'key', 'value'
    ];

    public function user() {
        return belongsTo('App\User');
    }
}
