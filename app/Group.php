<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * Return all members
     */
    public function users() {
        return $this->belongsToMany('App\User');
    }

    /**
     * Return the group own nodes
     */
    public function getOwnNodes() {
        return $this->hasMany('App\Node', 'group_owner_id');
    }
}
