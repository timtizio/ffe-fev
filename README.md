    .    
    ├── app                     => Application Classes (at root, the models class)    
    │   └── Http    
    │       ├── Controllers     => Application Controllers    
    │       └── Middleware      => Application Middleware    
    ├── database    
    │   └── migrations          => Migrations files (Database operation like creating table)    
    ├── public    
    │   ├── css                 => Css files    
    │   └── js                  => JS files    
    ├── resources    
    │   └── views               => All views (at root, the front-office views)    
    │       ├── admin           => Admin views    
    │       ├── auth            => Auth views (autogenerate by laravel)    
    │       └── layouts         => Layouts of application (one for the front-office and one for back-office)    
    ├── routes    
    │   └── web.php             => Routes application    
    ├── tests                   => Test directory     
    │
    └── fakeData.md             => Fake data for testing () 
 
Liens vers le wiki officiel : https://gitlab.com/timtizio/framework-files-exchange/wikis/home    
Liens vers le fichiers fakeData : https://gitlab.com/timtizio/ffe-fev/blob/master/fakeData.md    
