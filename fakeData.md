Fake data for testing
=====================

User :
------

| Name      | email         | password  | Note              |
|-----------|---------------|-----------|-------------------|
| lambda    | lambda@ffe.fr | lambda    | Lambda user       |
| admin     | admin@ffe.fr  | admin     | Application admin |

Groups :
--------

| Name      | Members names             | Note              |
|-----------|---------------------------|-------------------|
| Group 1   | lambda, admin             |                   |
| Group 2   | lambda                    |                   |

Nodes :
-------

| Name          | Type      | Parent        | Path          | User Owner    | Group Owner   | Permission    |
|---------------|-----------|---------------|---------------|---------------|---------------|---------------|
| file1         | File      | NULL          | real/path     | lambda        | Group 1       | 644           |
| directory1    | Directory | NULL          | NULL          | lambda        | Goupe 1       | 775           |
| file2         | File      | directory1    | real/path     | lambda        | Groupe 1      | 644           |
| admin-dir1    | Directory | NULL          | NULL          | admin         | Groupe 3      | 755           |
| admin-file1   | File      | admin-dir1    | real/path     | admin         | Groupe 3      | 660           |
| link1         | Link File | directory1    |real/path/file1| lambda        | Groupe 1      | 644           |
