<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\MetaUser;

class MetaUserTest extends TestCase
{

    /**
     * A basic test who check isAdmin() function
     * When the user is admin
     */
    public function testUserIsAdmin() {
        $user = User::create([
            'name' => 'username',
            'email' => 'username@email.fr',
            'password' => 'password',
        ]);

        $metaData = MetaUser::create([
            'user_id' => $user->id,
            'key' => 'isAdmin',
            'value' => 'true',
        ]);

        $userFind = User::where('name', 'username')->first();

        $this->assertTrue($userFind->isAdmin());

        $userFind->delete();
        $metaData->delete();
    }

    /**
     * A basic test who check isAdmin() function
     * when the user is not admin
     */
    public function testUserIsNotAdmin() {
        $user = User::create([
            'name' => 'username2',
            'email' => 'username2@email.fr',
            'password' => 'password',
        ]);

        $metaData = MetaUser::create([
            'user_id' => $user->id,
            'key' => 'isAdmin',
            'value' => 'false',
        ]);

        $userFind = User::where('name', 'username2')->first();

        $this->assertFalse($userFind->isAdmin());

        $userFind->delete();
        $metaData->delete();
    }

    /**
     * A basic test who check isAdmin function
     * when the value contain an error.
     * The returned value expected is false
     */
    public function testUserIsAdminError() {
        $user = User::create([
            'name' => 'username3',
            'email' => 'username3@email.fr',
            'password' => 'password',
        ]);

        $metaData = MetaUser::create([
            'user_id' => $user->id,
            'key' => 'isAdmin',
            'value' => 'truee', // An error
        ]);

        $userFind = User::where('name', 'username3')->first();

        $this->assertFalse($userFind->isAdmin());

        $userFind->delete();
        $metaData->delete();
    }

    /**
     * Basic test who check isAdmin function
     * when the MetaUser value is not set
     * The returned value expected is false
     */
    public function testUserIsAdminWithoutMetaUser() {
        $user = User::create([
            'name' => 'username4',
            'email' => 'username4@email.fr',
            'password' => 'password',
        ]);

        $userFind = User::where('name', 'username4')->first();

        $this->assertFalse($userFind->isAdmin());

        $userFind->delete();
    }

}
