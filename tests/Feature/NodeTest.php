<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Group;
use App\Node;

class NodeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testOwnMethods() {
        $user1 = User::create([
            "name" => "user1",
            "email" => "user1@email.fr",
            "password" => "password",
        ]);

        $group1 = Group::Create([
            "name" => "Group1",
        ]);

        $user1->groups()->attach($group1->id);

        $nodes1 = Node::create([
            'name' => 'fileexample',
            'type' => 'F',
            'path' => 'real/path',
            'user_owner_id' => $user1->id,
            'group_owner_id' => $group1->id,
            'permissions' => '664',
        ]);

        $nodeFind = Node::where('name', 'fileexample')->firstOrFail();

        $userOwner = $nodeFind->getUserOwner();
        $this->assertEquals($user1->id, $userOwner->id);

        $groupOwner = $nodeFind->getGroupOwner();
        $this->assertEquals($group1->id, $groupOwner->id);

        $this->assertEquals($user1->getOwnNodes()->where('name', 'fileexample')->firstOrFail()->id, $nodeFind->id); 
        $this->assertEquals($group1->getOwnNodes()->where('name', 'fileexample')->firstOrFail()->id, $nodeFind->id); 

        $nodeFind->delete();
        $user1->delete();
        $group1->delete();
    }

    public function testParentChildrenMethods() {
        $user1 = User::create([
            "name" => "user1",
            "email" => "user1@email.fr",
            "password" => "password",
        ]);

        $group1 = Group::Create([
            "name" => "Group1",
        ]);

        $user1->groups()->attach($group1->id);

        $directory1 = Node::create([
            'name' => 'direxample',
            'type' => 'D',
            'user_owner_id' => $user1->id,
            'group_owner_id' => $group1->id,
            'permissions' => '775',
        ]);

        $nodes1 = Node::create([
            'name' => 'fileexample',
            'type' => 'F',
            'parent_id' => $directory1->id,
            'path' => 'real/path',
            'user_owner_id' => $user1->id,
            'group_owner_id' => $group1->id,
            'permissions' => '664',
        ]);

        $this->assertEquals($directory1->id, $nodes1->getParent()->id);
        $this->assertEquals($nodes1->id, $directory1->getChildren()->first()->id);

        $nodes1->delete();
        $directory1->delete();
        $user1->delete();
        $group1->delete();
    }


}
