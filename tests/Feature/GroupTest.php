<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Group;

class GroupTest extends TestCase
{

    /**
     * A basic test who check the user group assignation
     */
    public function testUserAssignation() {
        $user1 = User::create([
            "name" => "user1",
            "email" => "user1@email.fr",
            "password" => "password",
        ]);

        $group1 = Group::Create([
            "name" => "Group1",
        ]);

        $userFind = User::where('name', 'user1')->first();

        $userFind->groups()->attach($group1->id);
        $this->assertEquals(1, $group1->users()->count());

        $userFind->groups()->detach($group1->id);
        $this->assertEquals(0, $group1->users()->count());

        $userFind->delete();
        $group1->delete();
    }

}

