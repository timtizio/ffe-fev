<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\User;
use App\Group;
use App\Node;

class SeedTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testUserSeed() {
        $admin = User::where('name', 'admin')->firstOrFail();

        $this->assertEquals('admin@ffe.fr', $admin->email);
        $this->assertTrue($admin->isAdmin());

        $lambda = User::where('name', 'lambda')->firstOrFail();

        $this->assertEquals('lambda@ffe.fr', $lambda->email);
        $this->assertFalse($lambda->isAdmin());
    }

    public function testGroupsSeed() {
        $group1 = Group::where('name', "Group 1")->firstOrFail();

        $subset = $group1->users()->get()->map(function ($user) {
            return $user->only('name');
        });
        
        // dd($group1->users()->get()->toArray());
        
        $this->assertContains(['name' => 'admin', 'name' => 'lambda'], $subset);

        $group2 = Group::where('name', "Group 2")->firstOrFail();

        $subset = $group1->users()->get()->map(function ($user) {
            return $user->only('name');
        });

        $this->assertContains(['name' => 'lambda'], $subset);
    }

    public function testNodesSeed() {
        $file1 = Node::where('name', 'file1')->firstOrFail();

        $this->assertEquals('F', $file1->type);
        $this->assertEquals(null, $file1->getParent());
        $this->assertEquals('lambda', $file1->getUserOwner()->name);
        $this->assertEquals('Group 1', $file1->getGroupOwner()->name);

        $directory1 = Node::where('name', 'directory1')->firstOrFail();

        $this->assertEquals('D', $directory1->type);
        $this->assertEquals(null, $directory1->getParent());
        $this->assertEquals('lambda', $directory1->getUserOwner()->name);
        $this->assertEquals('Group 1', $directory1->getGroupOwner()->name);

        $file2 = Node::where('name', 'file2')->firstOrFail();

        $this->assertEquals('F', $file2->type);
        $this->assertEquals($directory1->id, $file2->getParent()->id);
        $this->assertEquals('lambda', $file2->getUserOwner()->name);
        $this->assertEquals('Group 1', $file2->getGroupOwner()->name);

        $adminDir = Node::where('name', 'admin-dir1')->firstOrFail();

        $this->assertEquals('D', $adminDir->type);
        $this->assertEquals(null, $adminDir->getParent());
        $this->assertEquals('admin', $adminDir->getUserOwner()->name);
        $this->assertEquals('Group 3', $adminDir->getGroupOwner()->name);

        $adminFile1 = Node::where('name', 'admin-file1')->firstOrFail();

        $this->assertEquals('F', $adminFile1->type);
        $this->assertEquals($adminDir->id, $adminFile1->getParent()->id);
        $this->assertEquals('admin', $adminFile1->getUserOwner()->name);
        $this->assertEquals('Group 3', $adminFile1->getGroupOwner()->name);


    }

}

